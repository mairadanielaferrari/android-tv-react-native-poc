import React from 'react';
import { StyleSheet, Text, View, Image, Linking, Button,TouchableHighlight } from 'react-native';
import WebServiceHandler from 'react-native-web-service-handler';
import { StackNavigator } from 'react-navigation';
import VideoPlayer from 'react-native-video-controls';
import GPlayer from 'react-native-giraffe-player'

export default class HomeScreen extends React.Component {	  
constructor(){
     super();
     this.state = {
       ip: null,
     }	
	 this.getIp();
   }
   
   componentDidMount(){
	 GPlayer.setFullScreenOnly(true);
     GPlayer.setShowNavIcon(true);
   }
    
	getIp(){
      WebServiceHandler.get('https://ipinfo.io/',{'Accept':'application/json'},null)
		.then((val)=>{
		   console.log('callapi:'+ JSON.stringify(val));
           this.setState({ip:val.ip})			
         })
         .catch((error) => console.log('callapi:'+ JSON.stringify(error)));
	}
	
  render() {    
	  let logo = {
		uri: 'http://brandmark.io/logo-rank/random/apple.png'
	};	

    return (		   
	<View style={styles.mainContainer}>
			<View style={styles.headerContainer}>
				<Image style={styles.logo} source={logo} resizeMode="contain" />	
				<Text style={styles.ip} > IP : {this.state.ip}</Text>				
			</View>			   
			<View style={styles.videoContainer}>			
			   <Button title="Play Video" onPress={() => { GPlayer.play('http://cds.y5w8j4a9.hwcdn.net/zlivingusa/index.m3u8'); }}/>                 
			</View>
	</View>
    );
  }
}

const styles = StyleSheet.create({  
  videoContainer: {
	flex: 8,
	justifyContent:'center',
	alignItems: 'center',	
  },  
  
  mainContainer: {
    width:'100%',
	height:'100%',
	flex: 10,
	flexDirection:'column',		
	backgroundColor: 'white',
  },    
  headerContainer: {
    flex: 1,	
	flexDirection:'row',
	justifyContent: 'flex-start',			
	alignContent:'flex-start',
  },
  logo:{
	flex:1,
	height: '100%',
	width: '50%',				
  },
  ip:{	
	flex:12,    
	textAlign:'right',	
	backgroundColor: 'white',	
	paddingRight:'2%',		
	paddingTop:'2%',
  },
});

