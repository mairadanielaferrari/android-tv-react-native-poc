#How to setup development environment on a pristine machine?

## Dependencies

1. Please install Node >=6, Python 2 and JDK >= 8
2. Install the react native CLI using npm -> **npm install -g react-native-cli**

## Android Environment		

1. Install Android Studio

     [Download and install Android Studio](https://developer.android.com/studio/index.html). Choose a "Custom" setup when prompted to select an installation type. Make sure the boxes next to all of the following are checked:

     * Android SDK
     * Android SDK Platform
     * Performance (Intel ® HAXM)
     * Android Virtual Device

Then, click "Next" to install all of these components. Once setup has finalized and you're presented with the Welcome screen, proceed to the next step.

2. Install the Android SDK

Android Studio installs the latest Android SDK by default. Building a React Native app with native code, however, requires the **Android 6.0 (Marshmallow) SDK** in particular. Additional Android SDKs can be installed through the SDK Manager in Android Studio.

The SDK Manager can be accessed from the "Welcome to Android Studio" screen. Click on "Configure", then select "SDK Manager".

![GettingStartedAndroidStudioWelcomeWindows](images/GettingStartedAndroidStudioWelcomeWindows.png "Welcome Android")

Select the "SDK Platforms" tab from within the SDK Manager, then check the box next to "Show Package Details" in the bottom right corner. Look for and expand the Android 6.0 (Marshmallow) entry, then make sure the following items are all checked:

* Google APIs
* Android SDK Platform 23
* Intel x86 Atom_64 System Image
* Google APIs Intel x86 Atom_64 System Image

![GettingStartedAndroidSDKManagerWindows](images/GettingStartedAndroidSDKManagerWindows.png)

Next, select the "SDK Tools" tab and check the box next to "Show Package Details" here as well. Look for and expand the "Android SDK Build-Tools" entry, then make sure that 23.0.1 is selected.

![GettingStartedAndroidSDKManagerSDKToolsWindows](images/GettingStartedAndroidSDKManagerSDKToolsWindows.png)

Finally, click "Apply" to download and install the Android SDK and related build tools.

![GettingStartedAndroidSDKManagerInstallsWindows](images/GettingStartedAndroidSDKManagerInstallsWindows.png)

3. Configure the ANDROID_HOME environment variable

The React Native tools require some environment variables to be set up in order to build apps with native code.

Open the System pane under **System and Security** in the Control Panel, then click on **Change settings...** Open the **Advanced** tab and click on **Environment Variables...** Click on **New...** to create a new **ANDROID_HOME** user variable that points to the path to your Android SDK:

![GettingStartedAndroidEnvironmentVariableANDROID_HOME](images/GettingStartedAndroidEnvironmentVariableANDROID_HOME.png)

The SDK is installed, by default, at the following location:

**c:\Users\YOUR_USERNAME\AppData\Local\Android\Sdk**

You can find the actual location of the SDK in the Android Studio "Preferences" dialog, under Appearance & Behavior → System Settings → Android SDK.

Open a new Command Prompt window to ensure the new environment variable is loaded before proceeding to the next step.

#How to deploy the application to an android tv for testing purposes  

## Preparing the Android Device

You will need an Android device to run your React Native Android app. You can use an Android Virtual Device which allows you to emulate an Android device on your computer.

### Using a virtual device
You can see the list of available Android Virtual Devices (AVDs) by opening the "AVD Manager" from within Android Studio. Look for an icon that looks like this:

![GettingStartedAndroidStudioAVD](imagee/GettingStartedAndroidStudioAVD.png)

If you have just installed Android Studio, you will likely need to create a new AVD. Select "Create Virtual Device...", then pick an Android TV device from the list and click "Next".

![GettingStartedCreateAVDWindows](images/GettingStartedCreateAVDWindows.png)

Select the "x86 Images" tab, then look for the **Marshmallow** API Level 23, x86_64 ABI image with a Android 6.0 (Google APIs) target.

Once downloaded, start it.

![GettingStartedCreateAVDx86Windows](images/GettingStartedCreateAVDx86Windows.png)

Add Platform-tools to the PATH environment variable in order to use the Android Device Bridge (adb)

**c:\Users\YOUR_USERNAME\AppData\Local\Android\Sdk\platform-tools**

#How to build the application?

1. Clone the Repository - **git clone https://mairadanielaferrari@bitbucket.org/mairadanielaferrari/android-tv-react-native-poc.git**
2. cd ReactNativePOC
3. execute **npm install**
4. execut **react-native run-android**

#How to publish the application to Google Play?

## How to configure the App?

[Instructions that I followed in order to configure the app be able to deploy it to Google Play](http://blog.tylerbuchea.com/react-native-publishing-an-android-app/)

## How to generate a new release APK?
cd android && ./gradlew assembleRelease 

## Where is my bundled app?
The file you want to upload to the Google Play Store is android/app/build/outputs/apk/app-release.apk

## How to update version number for each build?
In android/app/build.gradle increment:

versionCode 2  
versionName "2.0"  

## How to publish it?

[Follow the instructions below in order to submit the app to google play](https://help.swiftic.com/hc/en-us/articles/201581812-Submit-Your-App-to-Google-Play)